#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Copyright © 2016-2017, W. van Ham, Radboud University Nijmegen

Beer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Beer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sleelab.  If not, see <http://www.gnu.org/licenses/>.

Beer is a control interface for the Behringer X-AIR audio mixer.
It contains a small subset of the controls of X AIR Edit.
'''

from __future__ import print_function
import struct, argparse, sys, socket, numpy as np, math, time, re
import PyQt4.QtCore as QtCore
import PyQt4.QtGui as QtGui
import logging
from slider import Slider
import initialize


class Meters(QtCore.QThread):
	""" Extra connection to X-AIR for meters """
	receive = QtCore.pyqtSignal([list])
	def __init__(self, ip):
		super(Meters, self).__init__()
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
		self.socket.settimeout(0.1)
		self.ip = ip
		self.port = 10024
		
	def send(self, s):
		"""send to beringer, low level"""
		try:
			#print("send: !{}! {}".format(s.encode("hex"), s))
			return self.socket.sendto(s.encode('utf8'), (self.ip, self.port))
		except Exception as e:
			print("Exception in Meters.send:     ", e)
			logging.error("error sending: !{}! {}".format(s.encode("utf8"), s))
			
	def sendOsc(self, s, form, args):
		"""send in osc format to Behringer.
		all three arguments will be justified to a multiple of 4 bytes
		form is the format, an iterable consisting of isfb (int32, null terminated string, float32, blob)
		all variables are justified to a multiple of 4 bytes
		very python2!!! Will not work in Python 3!!!!
		"""
		s += "\0"             # zero termination of all strings
		s += "\0"*(-len(s)%4) # 32 bit padding
		s +=","
		ss = ""
		if len(form) != len(args):
			logging.error("format and argumentlist have different length: format: {}, arguments: {}".format(form, args)) 
		for i in range(min(len(form), len(args))):
			if form[i]=='i':
				s += "i"
				ss += struct.pack(">L", args[i])
			elif form[i]=='f':
				s += "f"
				ss += struct.pack(">f", args[i])
			elif form[i]=='s':
				s += "s"
				ss += args[i] + "\0"
				ss += "\0"*(-(len(ss))%4) # 32 bit padding of content string
			elif form[i]=='b':
				logging.error("sorry, blobs are not implemented yet")
			else:
				logging.error("sorry, format code not recognized")
		s += "\0"*(-len(s)%4) # 32 bit padding of format string
		return self.send(s+ss)
		
	def receiveOsc(self):
		data, addr = self.socket.recvfrom(1024)
		#print(data, addr)
		#print("receiveOSC received:   ", data)
		i = data.find(b",")
		dataTitle = data[:i].rstrip(b"\0")
		#print("title: {:s}".format(dataTitle))
		dataType = data[i+1]
		dataContent = data[i+4:]
		#print(" type: {:s}".format(dataType))
		if (dataType == "b") or (dataType == 98):
			#print(dataContent.encode('hex'))
			pass
		else:
			print("dataType not supported:   ", dataType, " Title:", dataTitle, " Content:", dataContent)
		return dataContent


	def run(self):
		self.running = True
		while self.running:
			self.sendOsc("/meters", "s", ("/meters/1",)) # request 10s of updates
			try:
				while self.running: # every 50 ms for 10 s
					d = self.receiveOsc()
					length = struct.unpack(">L", d[0:4])[0] # length of blob in bytes
					n = struct.unpack("<L", d[4:8])[0] # number of floats
					#print("{} {} {}".format(len(d), length, n))
					a = []
					for j in range(n):
						f = float(struct.unpack("<h", d[8+j*2:8+(j+1)*2])[0]+32768)/32768 # 0.0-1.0 lower quart is noise
						a.append(f)
					self.receive.emit(a) 
			except socket.timeout as e:
				logging.info("timeout caught: ", e)

		


class Main(QtGui.QMainWindow):
	inChannelNumber = (1, 2, 3)
	inChannelName = ("mic 1", "mic 2", "PC out")
	nIn = len(inChannelNumber)
	
	outName = ("phones 1", "PC in", "phones 2", "speakers")
	outNumber = (0, 1, 3, 5) # 0 is main, 1, 3, 5 are left part of stereo bus
	outMonitorNumber = (3, 12, 13, 14) # same four as above, but now as monitor (solo)
	nOut = len(outName)

	def __init__(self, args):
		super(Main, self).__init__()
		self.initUI(args)
		self.ip = ""
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
		self.port = 10024
		self.args = args
		QtCore.QTimer.singleShot(1, self.processCommandLine)
		
	def initUI(self, args):
		#contents
		#self.text.setStyleSheet('text-align: right; color:#a00000; background-color:#000000; font-size: 100pt; font-family: Sans-Serif;')
		central = QtGui.QWidget()
		layout = QtGui.QGridLayout()
		central.setLayout(layout)
		
		# connections grid
		self.c = []
		for i in range(self.nOut):
			self.c.append([])
			for j in range(self.nIn):
				self.c[i].append(QtGui.QCheckBox())
				self.c[i][j].stateChanged.connect(lambda a, ii=i, jj=j:self.setGroup(a/2, jj+1, self.outNumber[ii]))
				layout.addWidget(self.c[i][j], i, j)
		
		# input faders
		self.channel = []
		for i in range(self.nIn):
			self.channel.append(Slider(orientation=QtCore.Qt.Vertical, text=self.inChannelName[i]))
			self.channel[i].valueChangedFloat.connect(lambda a, ii=i:self.setChannel(a, ii+1))
			layout.addWidget(self.channel[i], self.nOut, i)

		# output faders
		self.bus = []
		for i in range(self.nOut):
			self.bus.append(Slider(orientation=QtCore.Qt.Horizontal))
			self.bus[i].valueChangedFloat.connect(lambda a, ii=i:self.setBus(a, self.outNumber[ii]))
			layout.addWidget(self.bus[i], i, self.nIn+1)
			layout.addWidget(QtGui.QLabel(self.outName[i]), i, self.nIn+2)
			
		# monitor
		monitorLabel = QtGui.QLabel("monitor:")
		monitorLabel.setStyleSheet ("qproperty-alignment: 'AlignRight | AlignVCenter'; padding: 3px 8px; margin-left:40px;"); 
		layout.addWidget(monitorLabel, 2, self.nIn+3)
		self.monitor = QtGui.QComboBox(self)
		for s in self.outName:
			self.monitor.addItem(s)
		layout.addWidget(self.monitor, 2, self.nIn+4)
		self.monitor.activated.connect(self.setMonitor)
			
		l1 = QtGui.QPushButton('get ip:')
		l1.setStyleSheet ("text-align: right; padding: 3px 10px; margin-left:40px;"); 
		l1.clicked.connect(self.getIp)
		self.ipLine = QtGui.QLineEdit()
		l3 = QtGui.QPushButton('use:')
		l3.setStyleSheet ("text-align: right; padding: 3px 10px; margin-left:40px;"); 
		l3.clicked.connect(self.useIp)
		self.ipLabel = QtGui.QLabel("x.x.x.x")
		
		if args.device is None:
		    layout.addWidget(l1, 0, self.nIn+3)
		    layout.addWidget(self.ipLine, 0, self.nIn+4)
		    layout.addWidget(l3, 1, self.nIn+3)
		    layout.addWidget(self.ipLabel, 1, self.nIn+4)
		
		self.setCentralWidget(central)
		
		# dialogs
		self.errorMessageDialog = QtGui.QErrorMessage(self)
		
		## menubar
		loadAction = QtGui.QAction(QtGui.QIcon('icon/load.png'), '&Load', self)
		loadAction.setShortcut('Ctrl+L')
		loadAction.setStatusTip('Load settings file')
		loadAction.triggered.connect(self.load)
		
		saveAction = QtGui.QAction(QtGui.QIcon('icon/save.png'), '&Save', self)
		saveAction.setShortcut('Ctrl+S')
		saveAction.setStatusTip('Save settings file')
		saveAction.triggered.connect(self.save)

		exitAction = QtGui.QAction(QtGui.QIcon('icon/quit.png'), '&Exit', self)
		exitAction.setShortcut('Ctrl+Q')
		exitAction.setStatusTip('Quit application')
		#exitAction.triggered.connect(qApp.quit)
		exitAction.triggered.connect(QtGui.QApplication.quit)
		
		fullIcon = QtGui.QIcon('icon/full.png')
		fullAction = QtGui.QAction(fullIcon, '&Full Screen', self)
		fullAction.setShortcut('ctrl+F')
		fullAction.setStatusTip('Toggle Full Screen')
		fullAction.triggered.connect(self.toggleFullScreen)

		# populate the menu bar
		menubar = self.menuBar()
		fileMenu = menubar.addMenu('&File')
		fileMenu.addAction(loadAction)
		fileMenu.addAction(saveAction)
		fileMenu.addAction(exitAction)

		# keyboard shortcuts
		self.addAction(loadAction)
		self.addAction(saveAction)
		self.addAction(exitAction)
		self.addAction(fullAction)

		self.statusBar().showMessage('Ready')
		self.setWindowTitle('Beer, controlling the Behringer X-Air')
		self.show()

	# following functions are all slots
	def load(self, event=None, fileName=None):
		if fileName==None:
			fileName = QtGui.QFileDialog.getOpenFileName(self, "Open file", filter="Settings file [.scn] (*.scn)")
		if fileName==None or fileName=="":
			QtGui.QMessageBox.question(self, 'Error', "File name is empty", QtGui.QMessageBox.Ok)
			return
		with open(fileName, "rb") as input:
			for line in input:
				#input:  /ch/01/mix ON +10.0 ON +0
				#m = re.match(r'/ch/(\d+)/mix (ON|OFF)\s+([\-\+\d\.o]+) (ON|OFF) [\+\-\d\.]+', line)
				m = re.match(b'/ch/(\d+)/mix (ON|OFF)\s+([\-\+\d\.o]+) (ON|OFF) [\+\-\d\.]+', line)
				if m:
					i = int(m.group(1))-1
					if(i<len(self.channel)):
						self.channel[i].setValueDb(m.group(3))
						#self.channel[i].valueChanged.emit(self.channel[i].value()) # force signal
				#output: /bus/1/mix ON  -3.2 OFF +0
				#m = re.match(r'/bus/(\d+)/mix (ON|OFF)\s+([\-\+\d\.o]+) (ON|OFF) [\+\-\d\.]+', line)
				m = re.match(b'/bus/(\d+)/mix (ON|OFF)\s+([\-\+\d\.o]+) (ON|OFF) [\+\-\d\.]+', line)
				if m:
					i = int(m.group(1))-1
					if(i<len(self.bus)):
						self.bus[i].setValueDb(m.group(3))
						#self.bus[i].valueChanged.emit(self.bus[i].value()) # force signal
				#link:   /ch/01/mix/01   -oo ON GRP
				#m = re.match(r'/ch/(\d+)/mix/(\d+)\s+[\-\+\d\.o]+ (ON|OFF) GRP', line)
				m = re.match(b'/ch/(\d+)/mix/(\d+)\s+[\-\+\d\.o]+ (ON|OFF) GRP', line)
				if m:
					i = int(m.group(2))-1
					j = int(m.group(1))-1
					if i<len(self.c) and j < len(self.c[i]):
						self.c[i][j].setChecked(m.group(3)==b'ON')
						# force signal, even if there was no change in state
						#self.c[i][j].stateChanged.emit(self.c[i][j].checkState())
		self.emitAll()

	def save(self, event=None, fileName=None):
		if fileName==None:
			fileName = QtGui.QFileDialog.getSaveFileName(self, "Save file", filter="Settings file [.scn](*.scn)")
		if fileName==None or fileName=="":
			QtGui.QMessageBox.question(self, 'Error', "File name is empty", QtGui.QMessageBox.Ok)
			return
		with open(fileName, "wb") as output:
			output.write(initialize.scn)
			for i, channel in enumerate(self.channel):
				x = channel.valueDb()
				if x==float("-Infinity"):
					output.write("/ch/{:02d}/mix ON   -oo ON +0\n".format(i+1).encode('utf8'))
				else:
					output.write("/ch/{:02d}/mix ON {:+2.1f} ON +0\n".format(i+1, x).encode('utf8'))
			for i, bus in enumerate(self.bus):
				x = bus.valueDb()
				if x==float("-Infinity"):
					output.write("/bus/{:d}/mix ON   -oo OFF +0\n".format(i+1).encode('utf8'))
				else:
					output.write("/bus/{:d}/mix ON {:+2.1f} OFF +0\n".format(i+1, x).encode('utf8'))
			for i, channel in enumerate(self.channel):
				for j, bus in enumerate(self.bus):
					output.write("/ch/{:02d}/mix/{:02d}   -oo {} GRP\n".
						format(i+1, j+1, ('OFF', 'ON')[self.c[j][i].isChecked()]).encode('utf8'))

	def emitAll(self):
		"""Emit all signals from ui elements"""
		for i in range(self.nIn):
			self.channel[i].slider.valueChanged.emit(self.channel[i].slider.value()) # force signal
		for i in range(self.nOut):
			self.bus[i].slider.valueChanged.emit(self.bus[i].slider.value()) # force signal
		for i in range(self.nOut):
			for j in range(self.nIn):
				self.c[i][j].stateChanged.emit(self.c[i][j].checkState())


	def getIp(self):
		""" get Behringer ip address
		todo: the three attempts could be done in parallel"""
		self.statusBar().showMessage("")
		#00:1e:c0:24:c1:9b, last three used for id, probably first three always the same
		
		# find broadcast ip of public ip address (the one that connects us to the internet)
		#sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		#sock.connect(("8.8.8.8", 53)) # google dns
		#publicIp = sock.getsockname()[0] # our ip address
		#broadcastIp = socket.inet_ntoa( socket.inet_aton(publicIp)[:3] + b'\xff' )
		#sock.close()
		behrsfound = False
		myIPs = socket.gethostbyname_ex(socket.gethostname())[2]
		if myIPs is not None:
			# set up socket
			self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
			self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
			self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
			self.socket.settimeout(2.0)
			data, addr = [],[]
			behrs = []
			for publicIp in myIPs:
				# attempt broadcast to local network
				try:
					broadcastIp = socket.inet_ntoa( socket.inet_aton(publicIp)[:3] + b'\xff' )
					print("trying: !! " + broadcastIp)
					self.socket.sendto(b'/xinfo', (broadcastIp, 10024))
					data, addr = self.socket.recvfrom(1024) # moet += zijn
					print(b"Received: ", data, addr)
					if b'XR18' in data:
						print("Appending: ", addr)
						behrs.append(addr)
				except Exception as e:
					print("No reponse on !!---->" + broadcastIp)
					print(e)
					pass
			print(behrs)
			behrsfound = (behrs is not None) and (len(behrs) > 0)
			if behrsfound:
				self.ipLine.setText(behrs[0][0])
			else:
				self.ipLine.setText("No Behringers found.")
		else:
			print("not connected to a network")
		return behrsfound
	
	def useIp(self, ip=None):
		""" initialize mixer with default settings then set our current settings"""
		try:
			self.meters.quit()
			print("Meters already connected")
		except Exception as e:
			print("Quitting meters:  ", e)
			pass

		if ip:
			self.ip = ip
		else:
			print("DisplayText:   ", self.ipLine.displayText())
			self.ip = self.ipLine.displayText()   #str(self.ipLine.displayText().toAscii()) # read from ui
		
		if not self.ip:
			self.statusBar().showMessage("no ip, cannot synchronize")
			return

		self.ipLabel.setText(self.ip)
		
		progress = QtGui.QProgressBar(self)
		self.statusBar().addWidget(progress)
		iProgress = 0
	
		## initialize
		## send default settings for XR-18
		lines = initialize.init.splitlines()
		progress.setMaximum(len(lines))
		print('Sending defaults..  ', len(lines))
		for line in lines:
			progress.setValue(iProgress)
			iProgress += 1
			#sent = self.send("".join([a.decode('hex') for a in line.split('|')]))
			try:
						#print("SENDING: ", (b"".join([a.encode('utf8') for a in line.split('|')])))
						sent = self.send(b"".join([a.encode('utf8') for a in line.split('|')]))
						#print("SENT:   ", sent)
			except Exception as e:
						print("SEND EXCEPTION: ", e)
			time.sleep(0.001)
			#print(sent, line)

		## send non default settings to XR-18
		# send our settings: busses are sub group, and enable these sub groups
		#/ch/16/mix/01   -oo OFF POSTEQ +0   ->  /ch/16/mix/01   -oo OFF GRP +0
		for i in range(1, 17): # channel
			for j in range(1, 7): # bus
				sent = self.sendOsc("/ch/{:02d}/mix/{:02d}/tap".format(i, j), "i", (5,)) # option 5 (subgroup)
				#self.sendOsc("/ch/{:02d}/mix/{:02d}/grpon".format(i, j), "i", (i==j,)) # subgroup on
				time.sleep(0.001)
				#print(sent, i,j)

		# stereolinks
		self.sendOsc("/config/chlink/3-4", "i", (1,))
		self.sendOsc("/config/buslink/1-2", "i", (1,))
		self.sendOsc("/config/buslink/3-4", "i", (1,))
		self.sendOsc("/config/buslink/5-6", "i", (1,)) 
		self.sendOsc("/headamp/01/gain", "f", (0.585,)) # 30 dB microphone preamp
		self.sendOsc("/headamp/02/gain", "f", (0.585,))

		# send current state of sliders and checkboxes
		self.emitAll()
		
		self.statusBar().removeWidget(progress)
		print("starting meters")
		self.meters = Meters(self.ip)
		self.meters.receive.connect(self.getData, QtCore.Qt.QueuedConnection)
		self.meters.start()

	def getData(self, a):
		minimum = 0.5 # do not show below this value
		self.channel[0].slider.setMeter(max(0.0, min(1.0, (a[0]-minimum)/(1.0-minimum))))
		self.channel[1].slider.setMeter(max(0.0, min(1.0, (a[1]-minimum)/(1.0-minimum))))
		self.channel[2].slider.setMeter(max(0.0, min(1.0, (a[2]-minimum)/(1.0-minimum))))
		#print("getData: {:f}".format(a[2]))
		
	def send(self, s):
		"""send to beringer, low level"""
		try:
			#print("send: !{}! {}".format(s.encode("hex"), s))
			return self.socket.sendto(s, (self.ip, self.port))
		except Exception as e:
			#logging.error("error sending: !{}! {}".format(s.encode("hex"), s))
			print("Exception in self.send: ", e)
			logging.error("error sending: !{}! {}".format(s, s))
			self.statusBar().showMessage("error sending: {}".format(e))
			
	def sendOsc(self, s, form, args):
		"""send in osc format to Behringer.
		all three arguments will be justified to a multiple of 4 bytes
		form is the format, an iterable consisting of isfb (int32, null terminated string, float32, blob)
		all variables are justified to a multiple of 4 bytes
		very python2!!! Will not work in Python 3!!!!
		"""
		try:
			#print("SendOSC:  ", "s: " ,s, "form: ", form, "args: ", args)
			s = s.encode('utf8')
			s += b"\0"             # zero termination of all strings
			s += b"\0"*(-len(s)%4) # 32 bit padding
			s += b","
			ss = b""
			if len(form) != len(args):
				#print("Length error")
				logging.error("format and argumentlist have different length: format: {}, arguments: {}".format(form, args)) 
			for i in range(min(len(form), len(args))):
				#print("Length OK")
				if form[i]=='i':
					s += b"i"
					#print("s:->", s, "args[i]:->", args[i])
					ss += struct.pack(b">L", int(args[i]))
				elif form[i]=='f':
					s += b"f"
					ss += struct.pack(b">f", args[i])
				elif form[i]=='s':
					s += b"s"
					ss += args[i] + b"\0"
					ss += b"\0"*(-(len(ss))%4) # 32 bit padding of content string
				elif form[i]=='b':
					print("BLOB error")
					logging.error("sorry, blobs are not implemented yet")
				else:
					print("format error")
					logging.error("sorry, format code not recognized")
			s += b"\0"*(-len(s)%4) # 32 bit padding of format string
			return self.send(s+ss)
		except Exception as e:
			print("Exception in sendOSC:--:  ",e)
		
	def setChannel(self, x, i=1):
		"""set fader i to value x """
		try:
			self.sendOsc("/ch/{:02d}/mix/fader".format(i), "f", (x,))
		except Exception as e:
			self.statusBar().showMessage("error sending to channel: {}".format(e))
		
	def setBus(self, x, i=1):
		"""set bus i or main to value x """
		try:
			if i == 0:
				self.sendOsc("/lr/mix/fader", "f", (x,))
			else:
				self.sendOsc("/bus/{:d}/mix/fader".format(i), "f", (x,))
		except Exception as e:
			self.statusBar().showMessage("error sending to bus main: {}".format(e))
			
	def setGroup(self, x, channel=1, group=1):
		""" set group in channel """
		try:
			#print (channel, group)
			if group == 0:
				myString = "/ch/{:02d}/mix/lr".format(channel), "i", (x,)
				#print('SendOSC: ', myString)
				self.sendOsc("/ch/{:02d}/mix/lr".format(channel), "i", (x,))
			else:
				myString = "/ch/{:02d}/mix/{:02d}/grpon".format(channel, group)
				#print('SendOSC: ', myString)
				self.sendOsc("/ch/{:02d}/mix/{:02d}/grpon".format(channel, group), "i", (x,))
		except Exception as e:
			print("EXCEPTION in setGroup:  ", e)
			self.statusBar().showMessage("error sending group to channel: {}".format(e))
			
	def setMonitor(self, i):
		self.sendOsc("/config/solo/source", "i", (self.outMonitorNumber[i],))

	def toggleFullScreen(self, event=None):
		if(self.isFullScreen()):
			self.showNormal()
		else:
			self.showFullScreen()


	def checkIp(self, ip):
		try:
			'''try to send /xinfo command to the ipaddress in ip and try to receive
			   a response''' 
			self.socket.settimeout(0.1)		
			self.socket.sendto(b'/xinfo', (ip, 10024))
			data, addr = self.socket.recvfrom(1024)
		except Exception as e:
			'''If an exception (i.e. a timeout) occured, there was no response from the device,
			   so we assume that no Behringer is available at the given address. Show error box and quit.'''
			print(e)
			QtGui.QMessageBox.critical(self, "Error", "Could not connect to device at " + ip)
			sys.exit()


	def processCommandLine(self):
		# process command line arguments (to be called with running event loop)
		args = self.args
		if args.fullscreen:
			self.toggleFullScreen()
		if args.device:
			'''args.device contains an ipaddress. First check if we can connect to it. 
			   If not, the program is terminated.'''
			self.checkIp(args.device)
			'''Use the ipaddres in args.device, since we can connect to it.'''
			self.useIp(args.device)
		else:
			if self.getIp():
				self.useIp()
			else:
				QtGui.QMessageBox.critical(self, "Error", "No Behringers found.\nIs Behringer turned on?\nIs the network connection to Behringer active?")
				sys.exit()
		if args.scene:
			self.load(fileName=args.scene)
			
		

def main(): 
	# parse command line arguments
	parser = argparse.ArgumentParser()
	parser.add_argument("-geometry", help="X11 option")
	parser.add_argument("-display", help="X11 option")
	parser.add_argument("-f", "--fullscreen", help="start in full screen mode")
	parser.add_argument("-d", "--device", help="connect to device")
	parser.add_argument("-s", "--scene", help="load scene file (only with --device)")
	args = parser.parse_args()
	
	
	# make application and main window
	a = QtGui.QApplication(sys.argv)
	a.setApplicationName("Beer")
	w = Main(args); 
	a.lastWindowClosed.connect(QtGui.QApplication.quit) # make upper right cross work
	sys.exit(a.exec_())  # enter main loop (the underscore prevents using the keyword)

if __name__ == '__main__':
	if sys.platform == "win32":
		from ctypes import *
		from ctypes.wintypes import DWORD
		class TIMECAPS(Structure):
				_fields_ = [("wPeriodMin",c_uint),
										("wPeriodMax",c_uint)
										]
	
		caps = TIMECAPS()
		windll.winmm.timeGetDevCaps(byref(caps), sizeof(caps))
		windll.winmm.timeBeginPeriod(caps.wPeriodMin) # set resolution in ms
	main()   
	if sys.platform == "win32":
	    windll.winmm.timeEndPeriod(caps.wPeriodMin)
