#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Copyright © 2016-2017, W. van Ham, Radboud University Nijmegen

Beer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Beer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sleelab.  If not, see <http://www.gnu.org/licenses/>.

Beer is a control interface for the Behringer X-AIR audio mixer.
'''

import numpy as np, math
import PyQt4.QtCore as QtCore
import PyQt4.QtGui as QtGui
class ValueSlider(QtGui.QSlider):
	"""Qslider with value labels at the tickmarks"""
	labels = (u"-∞", "-50", "-30", "-20", "-10", "-5", "0", "5", "10")
	def __init__(self, *args, **kwargs):
		super(ValueSlider, self).__init__(*args, **kwargs)
		self.meter = 0.0
	def minimumSizeHint(self):
		return QtCore.QSize(65, 100)
	def setMeter(self, x):
		#print("setmeter: {:6.3f}".format(x))
		if self.meter == 0.0 and x == 0:
			return # saves the update
		self.meter = x
		self.update()
	def paintEvent(self, event):
		super(ValueSlider, self).paintEvent(event)
		st = self.style()
		slider = QtGui.QStyleOptionSlider()
		slider.initFrom(self)
		length = st.pixelMetric(QtGui.QStyle.PM_SliderLength, slider)
		# length is the difference between the slidable length and the widget length on both kde and msw10
		# other PM values have quite different meanings on different platforms
		"""
		thickness = st.pixelMetric(QtGui.QStyle.PM_SliderThickness, slider)
		cthickness = st.pixelMetric(QtGui.QStyle.PM_SliderControlThickness, slider)
		available = st.pixelMetric(QtGui.QStyle.PM_SliderSpaceAvailable, slider) 
		offset = st.pixelMetric(QtGui.QStyle.PM_SliderTickmarkOffset, slider) 
		print("{}: available: {}, length: {}, thcikness: {}/{}, offset: {}, rect: {}".format(
			("horizontal", "vertical")[self.orientation()==QtCore.Qt.Vertical], 
			available,
			length,
			cthickness,
			thickness,
			offset,
			self.rect()
			))
		"""
		qp = QtGui.QPainter()
		qp.begin(self)
		
		# draw VU meter
		qp.setPen(QtGui.QPen(QtCore.Qt.green, 2, QtCore.Qt.SolidLine, QtCore.Qt.FlatCap));
		center = ((self.rect().left()+self.rect().right())/2, (self.rect().bottom()+self.rect().top())/2)
		if self.orientation()==QtCore.Qt.Vertical:
			qp.drawLine(center[0], self.rect().bottom()-length/2, 
				center[0], self.rect().bottom()-length/2-int(self.meter*(self.height()-length))) # meter
		else:
			qp.drawLine(self.rect().left()+length/2, center[1], 
				self.rect().left()+length/2+int(self.meter*(self.width()-length)), center[1]) # meter
		
		# draw labels at ticks
		qp.setPen(QtGui.QPen(QtCore.Qt.black, 2, QtCore.Qt.SolidLine, QtCore.Qt.FlatCap));
		for i in range(len(self.labels)):
			if self.orientation()==QtCore.Qt.Vertical:
				if i%2 and self.height()<150 or self.height()<70:
					continue
				yText = st.sliderPositionFromValue(self.minimum(), self.maximum(), 
					self.minimum()+i*self.tickInterval(), self.height() - length)
				qp.drawText(QtCore.QPoint(self.rect().left(), self.rect().bottom()-length/2 + 5 -yText), self.labels[i]) # todo: QtCore.Qt.AlignRight
			else:
				if i%2 and self.width()<200 or self.width()<100:
					continue
				xText = st.sliderPositionFromValue(self.minimum(), self.maximum(), self.minimum()+i*self.tickInterval(), self.width() - length)
				qp.drawText(QtCore.QPoint(xText, self.rect().top()+3*length/4), self.labels[i])
		qp.end()

	


class Slider(QtGui.QWidget):
	"""Slider which represents the logarithmic scale of the Behringer faders"""
	valueChangedDb = QtCore.pyqtSignal(float)
	valueChangedFloat = QtCore.pyqtSignal(float) # 0-1 (mixer receives 32 bit float range 0-1 via OSC)
	# the sliders in X-AIR appear to be piecewise linear on a dB scale, -89.5 - 10
	dB = np.concatenate([[float("-Infinity")], 
		np.linspace(-79.5, -50, 9, endpoint=False),
		np.linspace(-50, -30, 10, endpoint=False),
		np.linspace(-30, -10, 20, endpoint=False),
		np.linspace(-10, 10, 41, endpoint=True),
		])
	
	def __init__(self, orientation=QtCore.Qt.Vertical, text=""):
		super(Slider, self).__init__()
		if orientation==QtCore.Qt.Vertical:
			self.layout = QtGui.QVBoxLayout()
		else:
			self.layout = QtGui.QHBoxLayout()
		self.setLayout(self.layout)
		self.layout.setMargin(0)
		self.slider = ValueSlider(orientation=orientation)
		self.layout.addWidget(self.slider)
		self.label = QtGui.QLabel(text)
		self.layout.addWidget(self.label)

		self.slider.setMinimum(0)  # corresponds to -infty dB
		self.slider.setMaximum(80) # corresponds to +10 dB
		self.slider.setTickInterval(10)
		self.slider.setTickPosition(self.slider.TicksLeft)
		self.slider.valueChanged.connect(self.valueChangedHandler)
	def valueChangedHandler(self, i):
		self.valueChangedDb.emit(self.dB[i])
		self.valueChangedFloat.emit(float(i)/80)
	
	def valueDb(self):
		return self.dB[self.slider.value()]
		
	def setValueDb(self, x):
		# -oo is Behringer style
		if x==b"-oo" or x==float("-Infinity"):
			self.slider.setValue(0)
			return
		if (type(x) == str) or (type(x) == bytes):
			x = float(x)
		#print("setValueDb: x==", x, self.dB, type(x))
		i = (np.abs(self.dB-x)).argmin() # index closest to x
		self.slider.setValue(i) # 0-80
